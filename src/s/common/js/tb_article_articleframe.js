'use strict';
import svg4everybody from 'svg4everybody/dist/svg4everybody.legacy.js';

svg4everybody();

import './tb_article_articleframe-section-slider-main/tb_article_articleframe-section-slider-main.js';


import './tb_article_articleframe-modal/tb_article_articleframe-modal.js';

import './tb_article_articleframe-readmore/tb_article_articleframe-readmore.js';


import './tb_article_articleframe-listmore/tb_article_articleframe-listmore.js';

import './lazyload/lazyload';
