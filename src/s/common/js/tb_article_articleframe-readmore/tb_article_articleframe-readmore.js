
$(function () {
  const slideOption = {
    speed:800
  }
  function toggleCoverClass($coverObj){
    if($coverObj[0]){
      if($coverObj.hasClass('is-opened')){
        $coverObj.removeClass('is-opened')
      }else{
        $coverObj.addClass('is-opened')
      }
    }
  }
  $('.tb_article_articleframe-readmore').on('click',function(e){
    e.preventDefault();
    var id = $(this).data('readmore');
    if(typeof id == 'undefined'){
      id= $(this).attr('href');
    }
    if ($(this).hasClass('is-opened')) {
      $(id).slideUp(slideOption.speed);
      $(this).removeClass('is-opened')
      toggleCoverClass($(this).parent('.tb_article_articleframe-more_content--cover'))
    }else{
      $(id).slideDown(slideOption.speed);
      $(id).find('[data-lazyload]').trigger('sporty')
      $(this).addClass('is-opened')
      toggleCoverClass($(this).parent('.tb_article_articleframe-more_content--cover'))
    }
  })
});
