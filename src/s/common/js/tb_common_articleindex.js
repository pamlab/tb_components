'use strict';
import svg4everybody from 'svg4everybody/dist/svg4everybody.legacy.js';

svg4everybody();


import './tb_article_articleframe-searchhotel/tb_article_articleframe-searchhotel.js';

import './tb_common_articleindex-readmore/tb_common_articleindex-readmore.js';
import './tb_common_articleindex-listmore/tb_common_articleindex-listmore.js';
import './tb_common_articleindex-yesno/tb_common_articleindex-yesno.js';

import './tb_common_articleindex-modal/tb_common_articleindex-modal.js';

import './lazyload/lazyload';
