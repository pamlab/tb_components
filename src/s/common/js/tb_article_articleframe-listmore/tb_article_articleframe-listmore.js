import './tb_article_articleframe-listmore.scss';

$(function () {
  const slideOption = {
    speed:600
  }
  function toggleCoverClass($coverObj){
    if($coverObj[0]){
      if($coverObj.hasClass('is-opened')){
        $coverObj.removeClass('is-opened')
      }else{
        $coverObj.addClass('is-opened')
      }
    }
  }
  $('.tb_article_articleframe-listmore').each(function(index){
    var initialview = $(this).data('initialview') - 1;
    if(typeof initialview == 'undefined') return;
    var $self = $(this)
    $self.children('li:gt('+initialview+')').wrapAll('<div class="tb_article_articleframe-more_content"></div>')
    var $wrap = $self.children('.tb_article_articleframe-more_content');
    var $morebutton = $(`
      <div class="tb_article_articleframe-more_content--cover">
    		<a href="#" class="tb_article_articleframe-buttonlist-readmore tb_article_articleframe-button--blue-more">もっと見る</a>
    	</div>
      `).insertAfter(this)
    $morebutton.children('.tb_article_articleframe-buttonlist-readmore').on('click',function(e){
      e.preventDefault();
      if ($(this).hasClass('is-opened')) {
        $wrap.slideUp(slideOption.speed)
        $self.removeClass('is-opened')
        $(this).removeClass('is-opened').text('もっと見る')
        toggleCoverClass($(this).parent('.tb_article_articleframe-more_content--cover'))
      }else{
        $wrap.slideDown(slideOption.speed)
        $wrap.find('[data-lazyload]').trigger('sporty')
        $self.addClass('is-opened')
        $(this).addClass('is-opened').text('閉じる')
        toggleCoverClass($(this).parent('.tb_article_articleframe-more_content--cover'))
      }
    });
  })
});
