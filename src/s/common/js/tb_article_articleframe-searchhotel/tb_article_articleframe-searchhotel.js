import './tb_article_articleframe-searchhotel.scss';

$(function () {
  if (!$('#searchhotel')[0]) return
  $(window).on('load',function(){
    var searchhotel = {}
    searchhotel.offset = $('#searchhotel').offset();
    searchhotel.height = $('#searchhotel').height();
    searchhotel.bottom = searchhotel.offset.top + searchhotel.height

    var $footer = $('#tabirai_footer_smt_pnlPCLink');
    var footer = {}
    footer.offset = $footer.offset();
    var windowH = $(window).height()

    var activeClass = 'is-active'
    var $button = $('.tb_common_articleindex-searchhotelbutton');
    $(window).on('scroll',function(){ //スクロールイベント
      var y = jQuery(window).scrollTop();
      if(y > searchhotel.bottom){
        $button.addClass(activeClass)
        if(y + windowH > footer.offset.top){
          $button.removeClass(activeClass)
        }
      }else{
        $button.removeClass(activeClass)
      }
    });
  })
});
