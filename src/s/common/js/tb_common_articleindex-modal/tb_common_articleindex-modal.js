require('jquery-colorbox');
import 'jquery-colorbox/example1/colorbox.css';
import './tb_common_articleindex-modal.scss';

$(function () {
  $('.tb_common_articleindex-modal--link').on('click',function(e){
    e.stopPropagation();
    var id = $(this).attr('href');
    $.colorbox(
      {
        href:$(id),
        inline:true,
        width:'100%',
        scrolling:false,
        closeButton:false,
        className:'tb_common_articleindex-modal--modal',
        onOpen:function(){
          var $closeBtn = $('<input type="button" class="tb_common_articleindex-modal-cboxClose">').appendTo('body');
          $closeBtn.on('click',function(){
            e.stopPropagation();
            $.colorbox.close();
          })
        },
        // onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
        // onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
        onCleanup:function(){
          $('.tb_common_articleindex-modal-cboxClose').remove();
         }
        // onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
      }
    );
  });
});
