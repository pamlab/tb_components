import './tb_common_articleindex-listmore.scss';

$(function () {
  const slideOption = {
    speed:400
  }
  function toggleCoverClass($coverObj){
    if($coverObj[0]){
      if($coverObj.hasClass('is-opened')){
        $coverObj.removeClass('is-opened')
      }else{
        $coverObj.addClass('is-opened')
      }
    }
  }
  $('.tb_common_articleindex-listmore').each(function(index){
    var initialview = $(this).data('initialview') - 1;
    if(typeof initialview == 'undefined') return;
    var $self = $(this)
    $self.children('*:gt('+initialview+')').wrapAll('<div class="tb_common_articleindex-more_content"></div>')
    var $wrap = $self.children('.tb_common_articleindex-more_content');
    var $morebutton = $(`
      <div class="tb_common_articleindex-more_content--cover">
    		<a href="#" class="tb_common_articleindex-buttonlist-readmore tb_common_articleindex-button--blue-more">もっと見る</a>
    	</div>
      `).insertAfter(this)
    $morebutton.children('.tb_common_articleindex-buttonlist-readmore').on('click',function(e){
      e.preventDefault();
      if ($(this).hasClass('is-opened')) {
        $wrap.slideUp(slideOption.speed)
        $self.removeClass('is-opened')
        $(this).removeClass('is-opened').text('もっと見る')
        toggleCoverClass($(this).parent('.tb_article_articleframe-more_content--cover'))
      }else{
        $wrap.slideDown(slideOption.speed)
        $wrap.find('[data-lazyload]').trigger('sporty')
        $self.addClass('is-opened')
        $(this).addClass('is-opened').text('閉じる')
        toggleCoverClass($(this).parent('.tb_article_articleframe-more_content--cover'))
      }
    });
  })
});
