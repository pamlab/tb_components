@mixin button {
  position: relative;
  border: 0;
  width: 100%;
  min-height: 50px;
  padding: 6px 30px 6px 40/670*100%;
  font-size: 15px;
  line-height: 1.3;
  display: flex;
  box-sizing: border-box;
  align-items: center;
  text-decoration: none;
  text-align: center;
  vertical-align: middle;
  &[type="button"]{
    display: inline-block;
    appearance: none;
    border-radius: 0;
    -webkit-box-sizing: content-box;
    -webkit-appearance: button;
    appearance: button;
    border: none;
    box-sizing: border-box;
    &::-webkit-search-decoration {
      display: none;
    }
    &::focus {
      outline-offset: -2px;
    }
  }
  >svg{
    width: 16px;
    height: 26px;
    position: absolute;
    right: 7px;
    top: 50%;
    transform: translateY(-50%);
  }
}
.#{$prefix}-button{
  &--orange{
    @include button;
    background: $colorOrange;
    color: #FFF!important;
    >svg{
      fill: #FFF;
    }
    &:hover{
      text-decoration: none;
    }
  }
  &--green{
    @include button;
    background: $colorGreen;
    color: #FFF!important;
    font-size: 15px;
    >svg{
      fill: #FFF;
    }
    &:hover{
      text-decoration: none;
    }
  }
  &--blue{
    @include button;
    background: $colorCoteDazur;
    color: #FFF!important;
    font-size: 17px;
    height: 50px;
    >svg{
      fill: #FFF;
      width: 16px;
      height: 26px;
      position: absolute;
      right: 7px;
      top: 50%;
      transform: translateY(-50%);
    }
  }
  &--blue-more{
    @include button;
    background: $colorCoteDazur;
    color: #FFF!important;
    height: 50px;
    justify-content: center;
    padding: 10px;
    font-size: 15px;
    &:after{
      content:"";
      width: 16px;
      height: 16px;
      background: url(#{$imgPath}svg/svgsprite/plus--blue.svg) 50% 50% no-repeat #FFF;
      background-size: 10px 10px;
      border-radius: 100%;
      display: inline-block;
      margin: 0 0 0 9px;
    }
    &.is-opened{
      &:after{
        background-image: url(#{$imgPath}svg/svgsprite/minus--blue.svg);
        background-size: 10px 3px;
      }
    }
  }
  &--gray-more{
    @include button;
    background: #eeeeee;
    color: $colorCoteDazur!important;
    height: 50px;
    font-size: 15px;
    padding: 6px 40/375*100% 6px 14/375*100%;
    font-weight: normal;
    &:after{
      content:"";
      width: 16px;
      height: 16px;
      background: url(#{$imgPath}svg/svgsprite/plus--blue.svg) 50% 50% no-repeat #FFF;
      background-size: 10px 10px;
      border-radius: 100%;
      display: inline-block;
      position: absolute;
      right: 16/375*100%;
      top:50%;
      transform: translateY(-50%);
    }
    &.is-opened{
      &:after{
        background-image: url(#{$imgPath}svg/svgsprite/minus--blue.svg);
        background-size: 10px 3px;
      }
    }
  }
  &--blue-line{
    @include button;
    border: solid 2px $colorCoteDazur;
    color: $colorCoteDazur!important;
    width: 100%;
    height: 40px;
    padding: 10px 40px 10px 20/670*100%;
    font-size: 15px;
    >svg{
      fill: $colorCoteDazur;
    }
    &:hover{
      text-decoration: none;
    }
  }
  &--blue-modal{
    a{
      @include button;
      font-weight: normal;
      background: $colorBlue;
      color: #FFF;
      text-align: left;
      font-size: 17px;
      padding: 7px 50/375*100% 11px 20/375*100%;
      line-height: 1.7;
      >svg{
        right: 20/375*100%;
        fill:#FFF;
      }
    }
  }
  &--modal{
    @include button;
    border: solid 1px #c1c1c1;
    color: #FFF;
    text-align: left;
    font-size: 17px;
    line-height: 1.7;
    display: table;
    table-layout: fixed;
    width: 100%;
    text-decoration: none;
    background: #FFF;
    color: #000;
    padding: 0;
    &-text,
    &-icon{
      display: table-cell;
      vertical-align: middle;
    }
    &-text{
      padding: 5px 19px;
      font-size: 15px;
    }
    &-icon{
      border-left: solid 1px #c1c1c1;
      width: 46px;
      background: #eee;
      text-align: center;
    }
    svg{
      right: 20/375*100%;
      fill:$colorCoteDazur;
      width: 16px;
      height: 16px;
    }
  }
}
// .#{$prefix}-button{
//   &--orange{
//     font-size: 17px;
//     padding: 10px;
//   }
// }
