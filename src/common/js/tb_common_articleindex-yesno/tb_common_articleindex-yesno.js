
$(function () {
  $('.tb_common_articleindex-yesno').each(function(index){
    var activeIndex = 0;
    var $children = $(this).children('.tb_common_articleindex-yesno--section');
    //$children.not(':eq('+activeIndex+')').hide();
    $children.eq(activeIndex).addClass('is-active');
    var $noButton = $(this).find('.tb_common_articleindex-yesno--button-no');
    $noButton.on('click',function(e){
      e.preventDefault();
      $children.eq(activeIndex).removeClass('is-active');
      activeIndex = activeIndex + 1;
      if(activeIndex + 1 <= $children.length){
        $children.eq(activeIndex).addClass('is-active');
      }else{
        activeIndex = 0;
        $children.eq(activeIndex).addClass('is-active');
      }
    })
  });
});
