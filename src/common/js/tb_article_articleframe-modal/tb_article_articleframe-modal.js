
require('jquery-colorbox');
import 'jquery-colorbox/example1/colorbox.css';
import './tb_article_articleframe-modal.scss';
$(function () {
  $('.tb_article_articleframe-modal--link').on('click',function(e){
    e.stopPropagation();
    var id = $(this).attr('href');
    $.colorbox(
      {
        href:$(id),
        inline:true,
        maxWidth:'1080px',
        scrolling:false,
        closeButton:false,
        speed:500,
        transition:"fade",
        className:'tb_article_articleframe-modal--modal',
        onOpen:function(){
          var $closeBtn = $('<input type="button" class="tb_article_articleframe-modal-cboxClose">').appendTo('body');
          $closeBtn.on('click',function(){
            e.stopPropagation();
            $.colorbox.close();
          })
        },
        // onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
        // onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
        onCleanup:function(){
          $('.tb_article_articleframe-modal-cboxClose').remove();
         }
        // onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
      }
    );
  });
});
