
require('slick-carousel');
import 'slick/slick.css';

$(function () {
  $('.tb_article_articleframe-section-slider-main').each(function(){
    var slickOption = {
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true
    }
    var $slick = $(this);
    var initialSlide = $slick.data('initialslide');
    if(typeof initialSlide !== 'undefined'){
      slickOption.initialSlide = initialSlide - 1;
    }else{
      slickOption.initialSlide = 0;
    }
    var slider = $(this).slick(slickOption);
    var $control = $(this).next('.tb_article_articleframe-section-slider-control');
    $control.children('li').each(function(index){
      $(this).on('click',function(){
        $slick.slick('slickGoTo',index)
      })
    });
    $control.children('li').eq(slickOption.initialSlide).addClass('slick-active');
    $slick.on('beforeChange',function(event, slick, currentSlide, nextSlide){
      $control.children('li').removeClass('slick-active');
      $control.children('li').eq(nextSlide).addClass('slick-active');
    })
  })
});
