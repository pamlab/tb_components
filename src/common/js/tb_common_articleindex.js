'use strict';
import svg4everybody from 'svg4everybody/dist/svg4everybody.legacy.js';

svg4everybody();

import './tb_common_articleindex-yesno/tb_common_articleindex-yesno';

import './lazyload/lazyload';

import './tb_common_articleindex-modal/tb_common_articleindex-modal';
