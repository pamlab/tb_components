
require('jquery-lazyload');
$(function() {

  $('[data-lazyload]').filter(function(){
    let option = {
      data_attribute : 'lazyload',
      effect : 'fadeIn',
      effect_speed: 1200,
      placeholder:'data:image/gif;base64,R0lGODlhAQABAIAAAM3NzQAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=='
    }
    if(typeof $(this).data('lazyload_placeholder') !== 'undefined') {
      option.placeholder = $(this).data('lazyload_placeholder')
    }
    if(typeof $(this).data('lazyload_sporty') !== 'undefined') {
      option.event = 'sporty'
      option.effect_speed = 0
    }
    $(this).lazyload(option);
  })
});
