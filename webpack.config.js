var webpack = require("webpack");
var path = require('path');
module.exports = {
  entry: {
      "common/js/tb_article_articleframe": "./src/common/js/tb_article_articleframe.js",
      "common/js/tb_common_articleindex": "./src/common/js/tb_common_articleindex.js",
      "s/common/js/tb_article_articleframe": "./src/s/common/js/tb_article_articleframe.js",
      "s/common/js/tb_common_articleindex": "./src/s/common/js/tb_common_articleindex.js"
  },
  output: {
    //path: __dirname + '/app/common/js/',
    path: __dirname + '/app/',
    filename: "[name].js",
		publicPath: '/app/',
  },
  externals: {
    jquery: 'jQuery'
  },
  resolve: {
    alias: {
      slick: path.resolve(__dirname, 'node_modules/slick-carousel/slick/'),
    }
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query:{
          presets: ['es2015']
        }
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      },
      { test: /\.scss$/, loaders: ['style-loader', 'css-loader', 'sass-loader'] },
      {
        test: /\.(jpg|png|gif)$/,
        loader: 'url-loader'
      },
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
  ]
}
