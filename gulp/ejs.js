'use strict';
const gulp     = require( 'gulp' );
const fs       = require( 'fs' );
const plumber  = require( 'gulp-plumber' );
const rename   = require( 'gulp-rename' );
const ejs      = require( 'gulp-ejs' );
const htmlhint = require('gulp-htmlhint');
var config = require('./config');
var watch = require('gulp-watch');



gulp.task('ejs',function(){
  var json = JSON.parse(fs.readFileSync("./pages.json"));
  gulp.src(config.ejs.src)
    //.pipe(ejs(config.ejs.settings))
    .pipe(ejs(json, {"ext": ".html"}))
    .pipe(plumber())
    .pipe(htmlhint('.htmlhintrc'))
    .pipe(htmlhint.reporter())
    .pipe(rename({extname: '.html'}))
    .pipe(gulp.dest(config.ejs.dest));
});

gulp.task('watch:ejs', () => {
    return watch(['./src/**/*.ejs'], () => {
        return gulp.start(['ejs']);
    });
});
