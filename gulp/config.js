// @file config.js
var projectName = 'tabirai';
var dest = './app'; // 出力先ディレクトリ
var src = './src';  // ソースディレクトリ
var doc = './docs';  // ドキュメントディレクトリ
module.exports = {
  // 出力先の指定
  dest: dest,
  // jsのビルド設定
  src:src,
  dest:dest,
  doc:doc,
  projectName:projectName,
  ejs: {
    src: [
      src + '/**/*.ejs',
      '!'+src + '/**/_*.ejs'
    ],
    dest: dest + '/',
    uglify: false,
    settings : {
      ext: '.html'
    }
  },
  css:{
    src: [
      '!'+src + '/**/_*.scss',
      src + '/**/*.scss',
    ],
    dest: dest,
    autoprefixer: {
      browsers: ["> 10%", "last 1 versions", "ie >= 11", "Firefox ESR", "safari >= 5", "ios_saf >= 8", "Android >= 4"]
    },
    sassOptions:{
      outputStyle:'expanded',//compressed
      sourceMap: true,
      sourceComments: false
    },
    minifier: false // minify化
  },
  js:{
    src: [
      src + '/**/*.js',
      '!'+src + '/js/plugin/*.js',
      '!'+src + '/js/plugin/**/*.js'
    ],
    plugin:{
      pc: [
        src + '/js/plugin/jquery-1.12.0.min.js',
        './node_modules/iscroll/build/iscroll.js',
        './node_modules/jquery-drawer/dist/js/drawer.js',
        src + '/js/plugin/flickity.pkgd.min.js',
        src + '/js/plugin/svg4everybody.min.js',
        './node_modules/fullpage.js/vendors/jquery.easings.min.js',
        './node_modules/fullpage.js/vendors/scrolloverflow.min.js',
        './node_modules/fullpage.js/dist/jquery.fullpage.min.js'
      ]
    },
    dest: dest + '/js'
  },
  image:{
    src: src + '/images/**/*.',
    dest: dest + '/images/',
  },
  jsDoc:{
    version:'1.0.0'
  }
}
