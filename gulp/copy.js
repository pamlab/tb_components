'use strict';
const gulp     = require( 'gulp' );
var config = require('./config');
var watch = require('gulp-watch');

var path = {
  src : "./src/",
  dist : "./app/"
}

gulp.task('copy',function(){
  gulp.src( path.src + '**/*.{ttf,woff,woff2}'  ).pipe( gulp.dest( 'app'     ) );
});

gulp.task('watch:copy', () => {
    return watch(path.src + '**/*.{ttf,woff,woff2}', () => {
        return gulp.start(['copy']);
    });
});
