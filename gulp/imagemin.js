'use strict';
var gulp     = require('gulp'),
    imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var config = require('./config');
var watch = require('gulp-watch');
var path = {
  src : "./src/",
  dist : "./app/"
}
var image_src = [
  '!'+path.src + '_**/*.{png,jpg,jpeg,gif,svg}',
  '!'+path.src + '**/_*.{png,jpg,jpeg,gif,svg}',
  path.src + '**/*.{png,jpg,jpeg,gif,svg}'
];
var image_dist = path.dist;


gulp.task("imagemin", function(){
    gulp.src( image_src )
    .pipe(imagemin(
        [pngquant({quality: '40-70', speed: 1})]
    ))
    .pipe(gulp.dest( image_dist ));
});



gulp.task('watch:imagemin', () => {
    return watch(image_src, () => {
        return gulp.start(['imagemin']);
    });
});
