var gulp = require('gulp');
var sass = require('gulp-sass');
var pleeease = require('gulp-pleeease');
var concat = require("gulp-concat");
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');
/*
pleeeaseのドキュメント
http://pleeease.io/docs/
*/
var config = require('./config');

var path = {
  src : "./src/",
  dist : "./app/"
}
var scss_src = path.src + '**/*.scss';


gulp.task('sass',function(){
  gulp.src(config.css.src)
    .pipe(sourcemaps.init())
    .pipe(sass(config.css.sassOptions))
    .pipe(pleeease({
        autoprefixer: config.css.autoprefixer,
        minifier: config.css.minifier,
        sourcemaps:true,
        // out:'style.min.css'
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.css.dest));
});


gulp.task('watch:sass', () => {
    return watch([scss_src], () => {
        return gulp.start(['sass']);
    });
});
